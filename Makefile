PLATFORM=$(shell uname)

all:
ifeq ($(PLATFORM),Darwin)
	$(CXX) -framework OpenCL -std=c++11 -O0 -g -o vector vector.cpp
else
	$(CXX) $(CPPFLAGS) -std=c++11 -O0 -g -o vector vector.cpp $(LDFLAGS) -lOpenCL
endif
