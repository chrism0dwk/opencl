/////////////////////////////////////////////////////
// Name: vector.cpp				   //
// Author: Chris Jewell <c.jewell@lancaster.ac.uk> //
// Created: 2015-07-03				   //
// Copyright: (c) Chris Jewell 2015		   //
// Purpose: OpenCL vector addition test		   //
/////////////////////////////////////////////////////

#define __CL_ENABLE_EXCEPTIONS

#include <cstdlib>
#include <iostream>
#include <string>
#include <stdexcept>
#include <algorithm>
#include <numeric>
#include "cl.hpp"

#define LOCAL_WORK_SIZE 64
#define VECSIZE 250*1024*1024


inline void checkErr(cl_int err, int line)
{
  if(err != CL_SUCCESS) {
    std::cerr << "CL ERROR " << err << " at line " << line << std::endl;
    exit(EXIT_FAILURE);
  }
}

#define CL_CHECK_ERR(e) checkErr(e, __LINE__)

std::string
deviceTypeName(cl_device_type dev)
{
  switch(dev) {
  case CL_DEVICE_TYPE_CPU:
    return "CPU";
  case CL_DEVICE_TYPE_GPU:
    return "GPU";
  case CL_DEVICE_TYPE_ACCELERATOR:
    return "Accelerator";
  case CL_DEVICE_TYPE_DEFAULT:
    return "Default";
#if __OPENCL_VERSION__ >= 120
      case CL_DEVICE_TYPE_CUSTOM:
      return "Custom";
#endif
  default:
    throw std::logic_error("Unknown device type");
  }
}

float randomNumber () { return (std::rand() % 100); }

#define STRINGIFY(A) #A

std::string clPrgm = STRINGIFY(
			       __kernel void vectoradd(__global float* a, __global const float* b, const int n)
			       {
				 
				 int tid = get_global_id(0);
				 if(tid < n)
				   a[tid] = sqrt(pow(a[tid],1.4f) + pow(b[tid], 1.4f));
			       }

			       __kernel void vectorfill(__global float* a, const int n)
			       {
				 int tid = get_global_id(0);
				 if(tid < n) a[tid] = tid+1;
			       }
			       
			       );

cl::Device
chooseCLDevice(const cl::Platform& platform)
{
    // Fetch CPU device
  std::cerr << "Checking CPU..." << std::flush;
  std::vector<cl::Device> cpuDevices;
  try {
    platform.getDevices(CL_DEVICE_TYPE_CPU, &cpuDevices);
    std::cerr << "found " << cpuDevices.size() << std::endl;
  } catch(cl::Error& e) {
    std::cerr << "failed. ";
    if(e.err() == CL_DEVICE_NOT_FOUND) std::cerr << "No devices present." << std::endl;
  }
  
  // Fetch GPU devices
  std::cerr << "Checking GPU..." << std::flush;
  std::vector< cl::Device> gpuDevices;
  try {
    platform.getDevices(CL_DEVICE_TYPE_GPU, &gpuDevices);
    std::cerr << "found " << gpuDevices.size() << std::endl;
  }
  catch(cl::Error& e) {
    std::cerr << "failed. ";
    if(e.err() == CL_DEVICE_NOT_FOUND) std::cerr << "No devices present." << std::endl;
  }

  // Stack devices
  std::vector<cl::Device> allDevices = gpuDevices;
  allDevices.insert(allDevices.end(), cpuDevices.begin(), cpuDevices.end());
  
  if(allDevices.size() == 0)
    throw std::runtime_error("No OpenCL compatible devices found. Exitting.");

  // Get with > 2000GB memory.  Order of devices chooses GPU first, CPU second
  cl::Device device;
  for(size_t i=0; i<allDevices.size(); ++i)
    {
      if(allDevices[i].getInfo<CL_DEVICE_GLOBAL_MEM_SIZE>() >= 2000*1024*1024) {
  	device = allDevices[i];
  	break;
      }
    }
  
  return device;
}


int main (int argc, char* argv[])
{


  // Get Platform
  std::vector< cl::Platform > platformList;
  try {
    cl::Platform::get(&platformList);
  }
  catch(cl::Error& e) {
    std::cerr << "No OpenCL platform found.  Exitting." << std::endl;
    exit(EXIT_FAILURE);
  }
  
  cl::Platform platform = platformList[0];
  std::cout << "Using platform: " << platform.getInfo<CL_PLATFORM_NAME>() << std::endl;

  cl::Device device = chooseCLDevice(platform);

  // Set up context
  cl::Context context(device);
  
  std::cout << "Using device: " << device.getInfo<CL_DEVICE_NAME>() << std::endl;
  std::cout << "Max compute units: " << device.getInfo<CL_DEVICE_MAX_COMPUTE_UNITS>() << std::endl;

  // CL Program
  cl::Program::Sources sources;
  sources.push_back({clPrgm.c_str(), clPrgm.length()});
  cl::Program program(context, sources);
  try {
    program.build({device});
  }
  catch(cl::Error& e) {
    std::cerr << "Error building program: " << program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(device) << std::endl;
    exit(EXIT_FAILURE);
  }
  
  // Device data
  cl::Buffer devA(context, CL_MEM_READ_WRITE, sizeof(float)*VECSIZE);
  cl::Buffer devB(context, CL_MEM_READ_WRITE, sizeof(float)*VECSIZE);

  // Command queue
  cl::CommandQueue queue(context, device, CL_QUEUE_PROFILING_ENABLE);

  // Launch kernels
  std::cout << "Make kernel..." << std::flush;
  typedef cl::make_kernel<cl::Buffer&, int> VectorFill;
  typedef cl::make_kernel<cl::Buffer&, cl::Buffer&, int> VectorAdd;
  std::function<VectorAdd::type_> vectoradd;
  std::function<VectorFill::type_> vectorfill;
  
  try {
    vectoradd = VectorAdd(program, "vectoradd");
    vectorfill = VectorFill(program, "vectorfill");
    std::cout << "Done" << std::endl;
  }
  catch (cl::Error& e) {
    std::cout << "Exception: " << e.what() << std::endl;
    exit(EXIT_FAILURE);
  }
  
  std::cout << "Launch kernels..." << std::flush;
  int gridDim = (VECSIZE + LOCAL_WORK_SIZE - 1) / LOCAL_WORK_SIZE;
  vectorfill(cl::EnqueueArgs(queue,cl::NullRange, cl::NDRange(VECSIZE), cl::NDRange(LOCAL_WORK_SIZE)), devA, VECSIZE);
  vectorfill(cl::EnqueueArgs(queue,cl::NullRange, cl::NDRange(VECSIZE), cl::NDRange(LOCAL_WORK_SIZE)), devB, VECSIZE);
  queue.finish();

  cl::Event event;
  event = vectoradd(cl::EnqueueArgs(queue,cl::NullRange, cl::NDRange(VECSIZE), cl::NDRange(LOCAL_WORK_SIZE)), devA, devB, VECSIZE);

  cl_ulong start,end;
  double total_time;
  event.wait();
  event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start);
  event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end);
  total_time = (end - start)/1000000.0;
  
  
  std::cout << "Done in " << total_time << "ms" << std::endl;

  std::cout << "Copy back data..." << std::flush;
  std::vector<float> res(VECSIZE);
  queue.enqueueReadBuffer(devA,CL_TRUE,0,sizeof(float)*VECSIZE,res.data());
  std::cout << "Result[0]: " << res[res.size()-1] << std::endl;
  std::cout << "Result: " << std::accumulate(res.begin(), res.end(), 0.0f) << std::endl;
  
  exit(EXIT_SUCCESS);
    
}
